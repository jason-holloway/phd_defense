function speckleDemo(blurSz)

%%  Speckle simulation

if nargin < 1
    blurSz = 'large';
end

switch lower(blurSz)
    
    case 'small'
        blurFactor = .8;
    case 'medium'
        blurFactor = .3;
    otherwise
        blurFactor = .07;
end


%%
% Set up camera
camera = cv.VideoCapture;
pause(1); % Necessary in some environment. See help cv.VideoCapture

im = camera.read;
imSz = size(im);
imSz(3) = [];
apDia = round(min(imSz)*blurFactor);

ph = rand(imSz,'single')*2*pi;


padVal = floor((imSz-apDia)/2)-1+mod(apDia,2); % even diameters need an offset of one pixel, odd diameters are fine as is
initPupil = padarray(fspecial('disk', apDia/2), padVal);
initPupil = padarray(initPupil,[1 1],'post');
initPupil(initPupil~=0)=1;
initPupil = single(initPupil);


disp('Speckle simulation. Press any key when done.');

%%
% Set up display window, and start the main loop

window = figure('KeyPressFcn',@(obj,evt)setappdata(obj,'flag',true));
setappdata(window,'flag',false);

F = @(x) fftshift(fft2(ifftshift(x)));
Ft = @(x) fftshift(ifft2(ifftshift(x)));

% Start main loop
while true
    % Grab and preprocess an image
    im = camera.read;
    %     im = cv.resize(im,0.5,0.5);
    gr = cv.cvtColor(im,'RGB2GRAY');
    
    % convert to single, add phase, take FFT, filter, and invert
    gr = im2single(gr);
    gr = gr.*exp(1i*ph);
    gr = Ft(F(gr).*initPupil);
    gr = sc(abs(gr).^2)*2;
    gr(:,:,[1 3]) = 0;
    
    
    % Draw results
    imshow(gr);
    drawnow;
    %     for i = 1:numel(boxes)
    %         rectangle('Position',boxes{i},'EdgeColor','g','LineWidth',2);
    %     end
    
    % Terminate if any user input
    flag = getappdata(window,'flag');
    if isempty(flag)||flag, break; end
    %     pause(0.1);
end

% Close
snapnow
close(window);
clear camera
