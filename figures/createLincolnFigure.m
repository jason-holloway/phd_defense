load lincoln im
im = im2double(im);
IM = fftshift(fft2(im));
crop1 = [149 212];
crop2 = [209 92];
IM1 = IM(crop1(1)+(1:crop1(2)),crop1(1)+(1:crop1(2)));
IM1 = padarray(IM1,[crop1(1)+1 crop1(1)+1],0);

IM2 = IM(crop2(1)+(1:crop2(2)),crop2(1)+(1:crop2(2)));
IM2 = padarray(IM2,[crop2(1)+1 crop2(1)+1],0);

im1 = ifft2(ifftshift(IM1));
im2 = ifft2(ifftshift(IM2));

im1 = abs(im1);
im2 = abs(im2);